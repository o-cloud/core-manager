FROM golang:1.16 AS builder

WORKDIR /usr/src

COPY ./go.mod ./go.sum ./

RUN go mod download 

COPY . .

RUN CGO_ENABLED=0 go build -o /go/bin/go-microservice-server

FROM alpine:3.13.6 as certs
RUN apk add -U --no-cache ca-certificates && update-ca-certificates

# Final image
FROM scratch
COPY ./config.yaml /etc/irtsb/
COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/bin/go-microservice-server /go/bin/go-microservice-server

ENTRYPOINT ["/go/bin/go-microservice-server"]

