package core

import (
	"context"
	"fmt"

	"gitlab.com/o-cloud/core-manager/api"
	"gitlab.com/o-cloud/core-manager/config"
	"go.opentelemetry.io/otel"
	"helm.sh/helm/v3/pkg/action"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/chart/loader"
	"helm.sh/helm/v3/pkg/release"
)

func (cm *CoreManager) DeployedCharts(ctx context.Context) ([]api.SimpleRelease, error) {
	ctx, span := otel.Tracer("core-manager/core").Start(ctx, "DeployedCharts")
	defer span.End()
	results := []api.SimpleRelease{}
	listAction := action.NewList(cm.actionConfig)
	rel, err := listAction.Run()
	if err != nil {
		return results, err
	}

	for _, r := range rel {
		if GetChart(r.Chart.Metadata.Name).ChartName == "" {
			continue
		}
		sr := api.SimpleRelease{
			Name:          r.Name,
			Namespace:     r.Namespace,
			Version:       r.Version,
			ChartMetadata: r.Chart.Metadata,
		}
		results = append(results, sr)
	}
	return results, nil
}

func (cm *CoreManager) GetModuleDetail(ctx context.Context, name string) (release.Release, error) {
	ctx, span := otel.Tracer("core-manager/core").Start(ctx, "GetModuleDetail")
	defer span.End()
	get := action.NewGet(cm.actionConfig)
	rel, err := get.Run(name)
	if err != nil {
		return *rel, err
	}

	return *rel, nil
}

//Actions
func (cm *CoreManager) DeployChart(ctx context.Context, parameters api.DeployParameters) error {
	ctx, span := otel.Tracer("core-manager/core").Start(ctx, "DeployChart")
	defer span.End()
	client := action.NewInstall(cm.actionConfig)
	client.ReleaseName = parameters.Name
	client.Namespace = parameters.Namespace
	chartRequested, err := cm.LocateChartFromId(parameters.ChartName, parameters.ChartVersion)

	if err != nil {
		return err
	}
	_, err = client.Run(chartRequested, parameters.Config)
	return err
}

func (cm *CoreManager) UpgradeRelease(ctx context.Context, name, version string, config map[string]interface{}) error {
	ctx, span := otel.Tracer("core-manager/core").Start(ctx, "UpgradeRelease")
	defer span.End()
	currentRelease, err := cm.GetModuleDetail(ctx, name)
	if err != nil {
		return fmt.Errorf("Error: couldn't get current release. %s", err.Error())
	}
	upgrade := action.NewUpgrade(cm.actionConfig)
	chartVersion := currentRelease.Chart.Metadata.Version
	if version != "" {
		chartVersion = version
	}
	chart, err := cm.LocateChartFromId(currentRelease.Chart.Metadata.Name, chartVersion)
	if err != nil {
		return err
	}
	_, err = upgrade.Run(name, chart, config)
	return err
}

func (cm *CoreManager) RemoveRelease(ctx context.Context, name string) error {
	ctx, span := otel.Tracer("core-manager/core").Start(ctx, "RemoveRelease")
	defer span.End()
	delete := action.NewUninstall(cm.actionConfig)
	_, err := delete.Run(name)
	return err
}

func GetChart(name string) config.Chart {
	for _, c := range config.Config.Charts {
		if c.ChartName == name {
			return c
		}
	}
	return config.Chart{}
}

func (cm *CoreManager) LocateChartFromId(name, version string) (*chart.Chart, error) {
	var ret *chart.Chart
	cpo := action.ChartPathOptions{}
	if version != "" {
		cpo.Version = version
	}
	cp, err := cpo.LocateChart(GetChart(name).FormatRepo(), cm.settings)
	if err != nil {
		return ret, err
	}
	ret, err = loader.Load(cp)
	return ret, err
}
