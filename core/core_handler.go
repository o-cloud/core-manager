package core

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/tbaehler/gin-keycloak/pkg/ginkeycloak"
	"gitlab.com/o-cloud/core-manager/api"
	"gitlab.com/o-cloud/core-manager/config"
)

type CoreHandler struct {
	Api api.ICoreManager
}

func NewCoreHandler() *CoreHandler {
	cs := CoreManager{}
	cs.InitHelmCli()
	return &CoreHandler{Api: &cs}
}

//Setup

func (C *CoreHandler) SetupRoute(router *gin.RouterGroup) {
	if config.KeycloakEnabled {
		log.Println("setup auth config", config.KeycloakConfig)
		router.Use(ginkeycloak.NewAccessBuilder(config.KeycloakConfig).
			RestrictButForRealm("admin").
			Build())
	}
	router.GET("/charts", C.ManagedCharts)
	router.GET("/charts/:name", C.ManagedChartDetail)
	router.GET("/charts/:name/versions", C.ManagedChartVersions)
	router.GET("/releases", C.DeployedCharts)
	router.POST("/releases", C.DeployChart)
	router.POST("/releases/", C.DeployChart)
	router.GET("/releases/:name", C.GetModuleDetail)
	router.PATCH("/releases/:name", C.UpgradeRelease)
	router.DELETE("/releases/:name", C.RemoveRelease)
}

//Handlers

func (C *CoreHandler) ManagedCharts(c *gin.Context) {
	result, err := C.Api.ManagedCharts(c.Request.Context())
	JSONErr(c, http.StatusOK, result, err)
}

func (C *CoreHandler) ManagedChartDetail(c *gin.Context) {
	detail, err := C.Api.ManagedChartDetail(c.Request.Context(), c.Param("name"))
	JSONErr(c, http.StatusOK, detail, err)
}
func (C *CoreHandler) ManagedChartVersions(c *gin.Context) {
	versions, err := C.Api.ManagedChartVersions(c.Request.Context(), c.Param("name"))
	JSONErr(c, http.StatusOK, versions, err)
}

func (ch *CoreHandler) DeployedCharts(c *gin.Context) {
	ctx := c.Request.Context()
	result, err := ch.Api.DeployedCharts(ctx)
	JSONErr(c, http.StatusOK, result, err)

}

func (ch *CoreHandler) GetModuleDetail(c *gin.Context) {
	name := c.Param("name")
	ctx := c.Request.Context()
	result, err := ch.Api.GetModuleDetail(ctx, name)
	JSONErr(c, http.StatusOK, result, err)
}

func (ch *CoreHandler) DeployChart(c *gin.Context) {
	var params api.DeployParameters
	c.BindJSON(&params)
	if params.Namespace == "" {
		if c.Param("namespace") != "" {
			params.Namespace = c.Param("namespace")
		} else {
			params.Namespace = config.Config.Namespace
		}
	}
	OKErr(c, ch.Api.DeployChart(c.Request.Context(), params))
}

func (ch *CoreHandler) UpgradeRelease(c *gin.Context) {
	var params map[string]interface{}
	c.BindJSON(&params)
	name := c.Param("name")
	ver := c.Query("version")
	OKErr(c, ch.Api.UpgradeRelease(c.Request.Context(), name, ver, params))
}

func (ch *CoreHandler) RemoveRelease(c *gin.Context) {
	OKErr(c, ch.Api.RemoveRelease(c.Request.Context(), c.Param("name")))
}

func JSONErr(c *gin.Context, status int, obj interface{}, err error) {
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	} else {
		c.JSON(status, obj)
	}
}

func OKErr(c *gin.Context, err error) {
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	} else {
		c.JSON(http.StatusOK, gin.H{"status": "OK"})
	}
}
