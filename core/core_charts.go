package core

import (
	"context"
	"log"
	"os"
	"path/filepath"
	"sort"

	"github.com/Masterminds/semver/v3"
	"github.com/pkg/errors"
	"gitlab.com/o-cloud/core-manager/config"
	"go.opentelemetry.io/otel"
	"helm.sh/helm/v3/cmd/helm/search"
	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/helmpath"
	"helm.sh/helm/v3/pkg/repo"
)

//Get info
func (cm *CoreManager) ManagedCharts(ctx context.Context) ([]chart.Metadata, error) {

	ctx, span := otel.Tracer("core-manager/core").Start(ctx, "ManagedCharts")
	defer span.End()
	charts := []chart.Metadata{}

	// Load the repositories.yaml
	rf, err := repo.LoadFile(cm.settings.RepositoryConfig)
	if isNotExist(err) || len(rf.Repositories) == 0 {
		return []chart.Metadata{}, errors.New("no repositories configured")
	}

	index := search.NewIndex()
	//TODO Put this index into cache because its SLOOOOOOOOOW as hell
	for _, re := range rf.Repositories {
		n := re.Name
		log.Println("searching in ", re.Name)
		f := filepath.Join(cm.settings.RepositoryCache, helmpath.CacheIndexFile(n))
		ind, err := repo.LoadIndexFile(f)
		if err != nil {
			log.Printf("Repo %q is corrupt or missing. Try 'helm repo update'.", n)
			log.Printf("%s", err)
			continue
		}
		index.AddRepo(n, ind, true)
	}
	results, err := index.Search("", 25, false)
	if err != nil {
		return []chart.Metadata{}, errors.New("Error during search")
	}
	foundNames := map[string]bool{}
	found := map[string]*search.Result{}
mainloop:
	for _, r := range results {

		if !foundNames[r.Name] {
			for _, managedChart := range config.Config.Charts {
				if r.Name == managedChart.FormatRepo() {
					foundNames[r.Name] = true
					found[r.Name] = r
					continue mainloop
				}
			}
			continue
		}
		nver, err := semver.NewVersion(r.Chart.Version)
		if err != nil {
			continue
		}
		fver, err := semver.NewVersion(found[r.Name].Chart.Version)
		if err != nil || nver.GreaterThan(fver) {
			found[r.Name] = r
		}
	}

	for _, r := range found {
		charts = append(charts, *r.Chart.Metadata)
	}
	return charts, nil
}

func (cm *CoreManager) ManagedChartDetail(ctx context.Context, name string) (chart.Chart, error) {
	ctx, span := otel.Tracer("core-manager/core").Start(ctx, "ManagedChartDetail")
	defer span.End()
	crt, err := cm.LocateChartFromId(name, "")
	if err != nil {
		return chart.Chart{}, err
	}
	return *crt, err
}

func (cm *CoreManager) ManagedChartVersions(ctx context.Context, name string) ([]*repo.ChartVersion, error) {
	ctx, span := otel.Tracer("core-manager/core").Start(ctx, "ManagedChartVersions")
	defer span.End()
	chartName := GetChart(name).FormatRepo()

	// Load the repositories.yaml
	rf, err := repo.LoadFile(cm.settings.RepositoryConfig)
	if isNotExist(err) || len(rf.Repositories) == 0 {
		return []*repo.ChartVersion{}, errors.New("no repositories configured")
	}

	index := search.NewIndex()
	for _, re := range rf.Repositories {
		n := re.Name
		f := filepath.Join(cm.settings.RepositoryCache, helmpath.CacheIndexFile(n))
		ind, err := repo.LoadIndexFile(f)
		if err != nil {
			log.Printf("Repo %q is corrupt or missing. Try 'helm repo update'.", n)
			log.Printf("%s", err)
			continue
		}
		index.AddRepo(n, ind, true)
	}
	se, err := index.Search(chartName, 25, false)
	results := []*repo.ChartVersion{}
	for _, r := range se {
		results = append(results, r.Chart)
	}
	sort.Slice(results, func(i, j int) bool {
		vi, _ := semver.NewVersion(results[i].Version)
		vj, _ := semver.NewVersion(results[j].Version)
		return vi.GreaterThan(vj)
	})
	return results, nil

}

func isNotExist(err error) bool {
	return os.IsNotExist(errors.Cause(err))
}
