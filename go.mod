module gitlab.com/o-cloud/core-manager

go 1.16

require (
	github.com/Masterminds/semver/v3 v3.1.1
	github.com/gin-gonic/gin v1.7.4
	github.com/gofrs/flock v0.8.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.8.1
	github.com/tbaehler/gin-keycloak v1.2.1
	gitlab.com/o-cloud/sbotel v0.0.0-20211022075011-9d04040dc1fb
	gitlab.com/o-cloud/sbotel/viperconf v0.0.0-20211022075011-9d04040dc1fb
	go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin v0.22.0
	go.opentelemetry.io/otel v1.0.0-RC2
	gopkg.in/yaml.v2 v2.4.0
	helm.sh/helm/v3 v3.6.3
	k8s.io/cli-runtime v0.22.1
	k8s.io/client-go v0.22.1
	rsc.io/letsencrypt v0.0.3 // indirect
)
