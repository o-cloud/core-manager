package config

import "testing"

func TestChartNameFormat(t *testing.T) {
	c := Chart{ChartName: "name", RepoName: "repo"}
	if c.FormatRepo() != "repo/name" {
		t.Errorf("Incorrect format")
	}
}
