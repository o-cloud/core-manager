package config

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
	"github.com/tbaehler/gin-keycloak/pkg/ginkeycloak"
	"gitlab.com/o-cloud/sbotel"
	"gitlab.com/o-cloud/sbotel/viperconf"
	"helm.sh/helm/v3/pkg/repo"
)

var (
	Config          ServiceConfig
	KeycloakEnabled bool
	KeycloakConfig  ginkeycloak.BuilderConfig
)

const (
	dockerConfigPath = "/etc/irtsb/"
	localConfigPath  = "./"
)

func Load() {

	viper.SetDefault("ServiceName", "core-manager")
	viper.BindEnv("ServiceName", "SERVICE_NAME")

	viper.SetConfigType("yaml")
	viper.AddConfigPath(dockerConfigPath)
	viper.AddConfigPath(localConfigPath)

	// Find and read the config file
	viper.SetConfigName("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Unable to read config file", err)
	}

	// Decode config into ServiceConfig
	if err := viper.Unmarshal(&Config); err != nil {
		log.Fatal("unable to decode configuration", err)
	}

	verifyChartDuplicate()

	//Config otel
	if config, err := viperconf.New().WithFile(dockerConfigPath + "otel.yaml").GetConfig(); err != nil {
		log.Print("Error reading otel config file", err)
	} else {
		config.ServiceName = viper.GetString("ServiceName")
		sbotel.InitConfiguration(config)
	}

	loadKeycloakConfig()

}

func loadKeycloakConfig() {
	var keyconf internalKeycloakConfig
	vip := viper.New()
	vip.SetConfigType("yaml")
	vip.AddConfigPath("/conf/keycloak/")
	vip.SetConfigName("auth")
	if err := vip.ReadInConfig(); err != nil {
		log.Println("Unable to read config file", err)
		return
	}
	if err := vip.Unmarshal(&keyconf); err != nil {
		log.Fatal("Unable to read config file", err)
		return
	}
	KeycloakEnabled = keyconf.EnableKeycloakAuth
	KeycloakConfig = ginkeycloak.BuilderConfig{
		Service: keyconf.ClientIdKeycloack,
		Url:     keyconf.UrlKeycloack,
		Realm:   keyconf.RealmKeycloack,
	}

}

type ServiceConfig struct {
	Server      ServerServiceConfig
	Repos       []repo.Entry
	Charts      []Chart
	Namespace   string
	ServiceName string
}

type ServerServiceConfig struct {
	ListenAddress string
}

type Chart struct {
	ChartName string
	RepoName  string
}

func (c Chart) FormatRepo() string {
	return fmt.Sprintf("%s/%s", c.RepoName, c.ChartName)
}

func verifyChartDuplicate() {
	for ic, ec := range Config.Charts {
		for in, en := range Config.Charts {
			if ic == in {
				continue
			}
			if ec.ChartName == en.ChartName {
				log.Fatal(fmt.Sprint("duplicate chart name not allowed", ec, en))
			}
		}
	}
}

type internalKeycloakConfig struct {
	EnableKeycloakAuth bool
	UrlKeycloack       string
	RealmKeycloack     string
	ClientIdKeycloack  string
}
