package api

import (
	"context"

	"helm.sh/helm/v3/pkg/chart"
	"helm.sh/helm/v3/pkg/release"
	"helm.sh/helm/v3/pkg/repo"
)

type ICoreManager interface {
	//Get info on managed charts
	ManagedCharts(ctx context.Context) ([]chart.Metadata, error)
	ManagedChartDetail(ctx context.Context, id string) (chart.Chart, error)
	ManagedChartVersions(ctx context.Context, id string) ([]*repo.ChartVersion, error)

	// get info on deployed charts
	DeployedCharts(ctx context.Context) ([]SimpleRelease, error)
	GetModuleDetail(ctx context.Context, name string) (release.Release, error)

	//Actions
	DeployChart(ctx context.Context, parameters DeployParameters) error
	UpgradeRelease(ctx context.Context, name, version string, config map[string]interface{}) error
	RemoveRelease(ctx context.Context, name string) error
}

type SimpleRelease struct {
	Name          string          `json:"name,omitempty"`
	Namespace     string          `json:"namespace,omitempty"`
	Version       int             `json:"version,omitempty"`
	ChartMetadata *chart.Metadata `json:"metadata,omitempty"`
}

type DeployParameters struct {
	Name         string                 `json:"name,omitempty"`
	Namespace    string                 `json:"namespace,omitempty"`
	ChartName    string                 `json:"chartName,omitempty"`
	ChartVersion string                 `json:"chartVersion,omitempty"`
	Flags        map[string]interface{} `json:"flags,omitempty"`
	Config       map[string]interface{} `json:"config,omitempty"`
}
