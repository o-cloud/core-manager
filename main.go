package main

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/tbaehler/gin-keycloak/pkg/ginkeycloak"
	"gitlab.com/o-cloud/core-manager/config"
	"gitlab.com/o-cloud/core-manager/core"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
)

func main() {
	config.Load()
	r := setupRouter()

	if err := r.Run(config.Config.Server.ListenAddress); err != nil {
		log.Println(err)
	}
}

func setupRouter() *gin.Engine {
	r := gin.Default()
	r.Use(ginkeycloak.RequestLogger([]string{"uid"}, "data"))
	r.Use(func(c *gin.Context) { c.Writer.Header().Set("Access-Control-Allow-Origin", "*") })
	r.Use(otelgin.Middleware("CatalogService"))

	log.Println("setup routes")
	core.NewCoreHandler().SetupRoute(r.Group("/api/core/manager"))

	return r
}
